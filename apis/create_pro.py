from lib.httprequest import BaseClient
from configs.config import Url


class CreatePro(BaseClient):
  def __init__(self):
    super().__init__()
    self.client = BaseClient()
    self.url = Url()

  def create_pro(self, data):
    return self.client.http_requests(self.url.create_pro, 'post', data=data, api_name='create_pro')

  def del_pro(self, data):
    return self.client.http_requests(self.url.del_pro, 'post', data=data, api_name='del_pro')
