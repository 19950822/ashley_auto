#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys
import logging.config

BASE_PATH = os.path.dirname(os.path.abspath(''))
sys.path.append(BASE_PATH)

conf_file = os.path.join(BASE_PATH, "configs", r"logging.conf")
log_path = os.path.join(BASE_PATH, "logs")
logging.config.fileConfig(conf_file, defaults={"log_path": log_path})
print()


def log_handler():
  """获取日志handler"""
  return logging.getLogger("Ashley_CODE")


log = log_handler()

if __name__ == '__main__':
  log.info("info")
  log.error("error")
