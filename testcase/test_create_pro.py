import pytest
import allure
from Ashley_code.apis.create_pro import CreatePro

import os

@allure.title('创建文件夹用例集')
@allure.feature('create_pro')
class TestCreatePro:
  def setup_class(self):

    print('testcase执行开始！')
    self.api = CreatePro()
    self.name = 'hello'

  @allure.story('不传name')
  @pytest.mark.SmokeTest
  @pytest.mark.P0
  def test_create_pro_001(self):
    data = {
      'parent_id': ''
    }
    res = self.api.create_pro(data=data)
    with pytest.raises(Exception):
      assert res['status'] != '0'

  @allure.story('name')
  @pytest.mark.SmokeTest
  @pytest.mark.P0
  def test_create_pro_002(self):
    data = {
      'name': self.name,
      'parent_id': ''
    }
    res = self.api.create_pro(data=data)
    assert res['status'] == '0'


  def teardown_class(self):
    print('执行结束！开始清理数据！')
    #self.api.del_pro(data={})




if __name__ == '__main__':
  pytest.main(['-s', '-v', '--alluredir', './result'])
  os.system('allure generate ./result -o ./result/report --clean')





