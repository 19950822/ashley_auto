def bubble_sort(arr):
  # 定义一个标志位，用于判断整个数组是否已经有序
  flag = False
  for i in range(len(arr) - 1):
    for j in range(len(arr) - 1 - i):
      if arr[j] > arr[j + 1]:
        # 如果两个相邻的数的大小顺序不对，则交换它们的位置
        arr[j], arr[j + 1] = arr[j + 1], arr[j]
        flag = True
    # 如果某次遍历过程中没有发生交换，则说明整个数组已经有序，可以直接退出循环
    if not flag:
      break
  return arr


import requests

login_user = {
  'domain': 'haizhi',
  'username': 'world',
  'password': ''
}
url = 'https://www.bdp.cn/api/user/login'
res = requests.post(url, login_user)
assert res.json()['status'] == '11'

print(res.json())
