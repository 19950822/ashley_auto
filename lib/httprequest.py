import requests
from configs.config import BaseUrl, Url, DefaultLoginUser
import traceback
from requests.adapters import HTTPAdapter
from lib.logger import log


class BaseClient:
  def __init__(self):
    self.cookie = self.get_cookie()

  def get_cookie(self):
    login_user = {
      'domain': DefaultLoginUser.domain,
      'username': DefaultLoginUser.usr,
      'password': DefaultLoginUser.pwd
    }
    try:
      res = requests.post(url=f'{BaseUrl.base_url}/{Url.login}', data=login_user)
      self.token = res.json()['result']['access_token']
      log.info('获取token成功，token：{0}'.format(self.token))
      return self.token
    except Exception:
      err = traceback.format_exc()
      log.error(err)

  def http_requests(self, url, method='post', params=None, data=None, api_name=None, **kwargs):
    """

    :param url: 请求url
    :param method: 请求方法
    :param params:url请求参数
    :param data:请求体参数
    :param api_name:接口名称
    :param kwargs:
    :return:
    """
    # 设置超时重试次数
    session = requests.Session()
    session.mount('https://', HTTPAdapter(max_retries=3))

    urls = f'{BaseUrl.base_url}/{url}'
    try:
      res = requests.request(url=urls, method=method, params=params, data=data,
                             headers={'cookie': f'access_token="{self.cookie}"'}, timeout=3)
      res_json = res.json()
      if res_json['status'] in ('0', '3', '6'):
        return res_json
      else:
        log.error('请求失败，响应json：{0}，trace_id：{1}'.format(res_json, res_json['trcid'], ))
        return None
    except requests.exceptions.RequestException as e:
      log.error("接口{0}请求失败3次无回应，错误信息：{1}".format(api_name, e))

    except Exception:
      err = traceback.format_exc()
      log.error('请求异常，错误信息{0}'.format(err))


if __name__ == '__main__':
  pass
